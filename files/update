#!/bin/bash

set -Eeuo pipefail # Exit on error
trap cleanup SIGINT SIGTERM ERR EXIT

ISSUCCESS=false
LASTSUCCESSFILE=/var/opt/lifi_last_successful_update

function msg() {
	echo -e "\e[1;33m[$(date +%H:%M:%S)] \e[0m${@}"
}

function info() {
	msg "\e[36mINFO: \e[0m${@}"
}

function err() {
	echo -ne "\n"
	msg "\e[31mERROR: \e[0m${@}" >&2
	echo -ne "\n"
}

function warn() {
	msg "\e[33mWARNING: \e[0m${@}" >&2
}

function die() {
	echo -ne "\n"
	msg "\e[31mFATAL ERROR: \e[0m${@}" >&2
	echo -ne "\n"
	exit 1
}

function cleanup() {
	trap - SIGINT SIGTERM ERR EXIT

	if [ "$ISSUCCESS" = "true" ]; then
		exit 0
	fi

	cat - <<EOF
   +----------------------------------------------------------------------+
   |          It seems an error occurred while running the updater.       |
   |           Please write a bug report on the Lifi Bug Tracker:         |
   |             https://gitlab.gwdg.de/lifi/lifi-bugs/-/issues           |
   +----------------------------------------------------------------------+
EOF
	if [ "${CLOSEONEXIT}" = "true" ]; then
		exit 1
	else
		cat -
	fi
}

CLOSEONEXIT=true
OFFLINEMODE=false
EXAMRESETMODE=false

# Ensure this is run on a Lifi VM
if [ "x$(hostname -s)y" != "xLifiVMy" ] && [ "x$(hostname -s)y" != "xlifiarmy" ] && [ "x$(hostname -s)y" != "xExamy" ]; then
	die "Detected execution on wrong System. Hostname is '$(hostname -s)' and not 'LifiVM', 'lifiarm' or 'Exam'."
fi

# Read arguments
if [ "$#" = 1 ] && [ "x${1}y" = "xfrom-launchery" ]; then
	# Let user close the window manually.
	CLOSEONEXIT=false
elif [ "$#" = 1 ] && [ "x${1}y" = "xremindery" ]; then
	if [ -f "${LASTSUCCESSFILE}" ]; then
		SECSINCELASTUPDATE="$(( $(date +%s) - $(cat "${LASTSUCCESSFILE}") ))"
	else
		SECSINCELASTUPDATE="0" # This should only occure during first installation.
	fi
	SECINWEEK=604800
	# Show reminder only if last successful update was more than 7 days ago.
	if [ $SECSINCELASTUPDATE -gt $SECINWEEK ] ; then
		export XDG_RUNTIME_DIR=/run/user/$(id -u)
		# Show reminder as notification for 20 seconds
		notify-send -u normal --app-name=lifi-update --icon=software-update-available "Lifi Updates Verfügbar" "Bitte starte das 'System Aktualisiern' Programm(hier klicken)." -t 20000
	fi
	ISSUCCESS=true
	exit 0
elif [ "$#" = 1 ] && [ "x${1}y" = "xoffliney" ]; then
	info "Using offline mode."
	OFFLINEMODE=true
elif [ "$#" = 1 ] && [ "x${1}y" = "xexam-resety" ]; then
	info "Using exam-reset mode."
	EXAMRESETMODE=true
	OFFLINEMODE=true
fi

pushd /opt/lifi >/dev/null

info "===>>> Updating Lifi... <<<==="


# Update Ansible Role Repo in /opt
#info "Getting latest Ansible Role"
if [ "${OFFLINEMODE}" = "false" ]; then
	if [ ! -f /opt/lifi/.dev ]; then
		sudo git fetch
		sudo git reset --hard HEAD
		sudo git clean -d -f
		sudo git merge origin/main
		sudo git submodule sync
		sudo git submodule init
		sudo git submodule update --recursive
	else
		warn "Detected dev mode."
		if [ -f /opt/lifi/.dev-update ]; then
			info "Updating current branch..."
			sudo git pull
			sudo git submodule sync
			sudo git submodule init
			sudo git submodule update --recursive
		fi
	fi
fi

# Print motd_start
cat motd_start

# Execute hotfix_start.sh
if [ -x hotfix_start.sh ]; then
	./hotfix_start.sh
fi

# Execute Ansible Role locally
function playbook() {
	cat - <<EOF
---
- name: Lifi Updater
  hosts: localhost
  connection: local
  roles:
    - /opt/lifi
EOF
	if [ -f /opt/exam/config.yaml ]; then
		cat - <<EOF
  vars_files:
    - /opt/exam/config.yaml
EOF
	fi
}
export ANSIBLE_RETRY_FILES_ENABLED="False"
export ANSIBLE_LOCALHOST_WARNING=false
if [ "${EXAMRESETMODE}" = "true" ]; then
	playbook | ansible-playbook --inventory /opt/lifi/inventory.yml --module-path /opt/lifi/ansible-gsetting --tags "exam-reset" /dev/stdin
elif [ "${OFFLINEMODE}" = "true" ]; then
	playbook | ansible-playbook --inventory /opt/lifi/inventory.yml --module-path /opt/lifi/ansible-gsetting --skip-tags "requires_internet" /dev/stdin
else
	playbook | ansible-playbook --inventory /opt/lifi/inventory.yml --module-path /opt/lifi/ansible-gsetting /dev/stdin
fi

# Execute hotfix_end.sh
if [ -x hotfix_end.sh ]; then
	./hotfix_end.sh
fi

# Print motd_end
cat motd_end

info "===>>> Lifi successfully updated. <<<==="
info "(You may need to restart the VM.)"

date +%s | sudo tee ${LASTSUCCESSFILE} >/dev/null

ISSUCCESS=true

popd >/dev/null

if [ "${CLOSEONEXIT}" = "true" ]; then
	exit 0
else
	info "You can close this window now."
	cat -
fi

