#!/bin/bash

pushd keyrings

# VSCodium
wget -qO - "https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg" | gpg --dearmor | dd of=vscodium-archive-keyring.gpg

# ppa:mozillateam https://launchpad.net/~mozillateam/+archive/ubuntu/ppa 
wget -qO - "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x0ab215679c571d1c8325275b9bdb3d89ce49ec21" | gpg --dearmor | dd of=mozillateam-ubuntu-ppa.gpg

# linuxmint (for chromium)
wget -qO - "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x302f0738f465c1535761f965a6616109451bbbf2" | gpg --dearmor | dd of=linuxmint-archive-keyring.gpg

popd

