Lifi Ansible Role
=================

Definiert den Status des Lifi VM Images.

Requirements
------------

SOLLTE NUR INNERHALB VON EINER LIFI VM AUSGEFÜHRT WERDEN!

THIS ROLE SHOULD ONLY EVERY BE EXECUTED INSIDE OF LIFI VMs!

License
-------

CC0 1.0 Universal

Author Information
------------------

Institut für Informatik
Fakultät für Mathematik und Informatik
Georg-August-Universität Göttingen
https://www.uni-goettingen.de/de/institut+f%c3%bcr+informatik/619480.html
